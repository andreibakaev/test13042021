const { ApolloServer, gql } = require('apollo-server');
const { v4: uuidv4 } = require('uuid');

const typeDefs = gql`
  type user {
    id: ID!
    email: String
    code: String
  }

  type Query {
    users: [user]
    user(input: ID!): user   
  }

  type Mutation {
    mailChange(id: ID, email: String!): user
    mailApply(id: ID!, email: String!, code: String!): Boolean
  }

`;

const users = [
  {
    id: genId(),
    email: 'test1@test.ru',
    code: genCode()
  },
  {
    id: genId(),
    email: 'test2@test.ru',
    code: genCode()
  }
];

const resolvers = {
  Query: {
    users: () => users,
    user: (root, { input }) => users.find(user => user.id === input)
  },
  Mutation: {
    mailChange: (root, args) => {
      const { id, email } = args;
      const code = genCode();
      const foundUser = id ? users.find(user => user.id === id) : null;
      if (foundUser) {
        foundUser = { ...foundUser, email, code };
        return { email, id: foundUser.id };
      } else {
        const newUser = {
          id: genId(),
          email,
          code
        };
        users.push(newUser);
        return { email, id: newUser.id };
      }
    },
    mailApply: (root, args) => {
      const { id, email, code } = args;
      const foundUser = users.find(user => user.id === id && user.email === email);
      if (!foundUser) {
        return false;
      }
      return foundUser.code === code;
    }
  }
}

const server = new ApolloServer({ typeDefs, resolvers });

server.listen().then(({ url }) => {
  console.log(`---apollo server---${url}`,);
})

function genCode() {
  const min = 0;
  const max = 9999;
  const len = 4;

  const rnd = Math.floor(Math.random() * (max - min + 1)) + min;
  return `${rnd}`.padStart(len, '0');
}

function genId() {
  return uuidv4();
}