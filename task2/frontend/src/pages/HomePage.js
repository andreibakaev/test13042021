
import React from 'react';
import styled from 'styled-components/macro';
import Header from '../components/Header';
import Verification from '../components/Verification';
import Help from '../components/Help';

import EmailDialog from '../components/EmailDialog';
import SuccessDialog from '../components/SuccessDialog';

const Container = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  position: relative;
`;

function HomePage() {
  const [pageState, setPageState] = React.useState('init');
  const [userState, setUserState] = React.useState({});

  React.useEffect(() => {
    setTimeout(() => setPageState('show_email_dialog'), 1000);
  }, []);

  const handleEmailChange = React.useCallback((data) => {
    setUserState(data);
    setPageState('show_success_dialog');
    setTimeout(() => setPageState('show_verification'), 2000);
  }, []);

  return (
    <Container>
      <Header />
      {
        pageState === 'show_email_dialog' && <EmailDialog onChangeEmail={handleEmailChange} userState={userState} />
      }
      {
        pageState === 'show_success_dialog' && <SuccessDialog />
      }
      {
        pageState === 'show_verification' && <Verification userState={userState} />
      }
      <Help />
    </Container>
  )
}

export default HomePage;