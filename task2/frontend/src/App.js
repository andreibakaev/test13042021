import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import { ApolloProvider } from '@apollo/client/react';

import { ThemeProvider } from 'styled-components/macro';
import { theme, GlobalStyle } from './theme';

import HomePage from './pages/HomePage';

const client = new ApolloClient({
  uri: 'http://localhost:4000',
  cache: new InMemoryCache()
});

function App() {
  return (
    <ApolloProvider client={client}>
      <ThemeProvider theme={theme}>
        <Router>
          <Switch>
            <Route path="/">
              <HomePage />
            </Route>
          </Switch>
        </Router>
        <GlobalStyle />
      </ThemeProvider>
    </ApolloProvider>
  )
}

export default App;

