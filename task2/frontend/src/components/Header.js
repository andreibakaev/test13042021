import styled from 'styled-components/macro';
import logo from '../images/logo.svg';

const Container = styled.header`
  height: 80px;
  width: 100%;
  background: #fff;
  box-shadow: inset 0px -1px 0px #E1E3E6;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const Logo = styled.div`
  padding-left: 120px;
`;

const Empty = styled.div`
  flex:1;
  backgroun: red;
`;

const LoginButton = styled.span`
  margin-right: 24px;
  cursor: pointer;
`;

const SignupButton = styled.span`
  color: #fff;
  margin-right: 120px;
  padding: 10px 16px;
  background: #0A6629;
  border-radius: 8px;
  cursor: pointer;
`;


function Header() {
  return (
    <Container>
      <Logo><img src={logo} alt="logo" /></Logo>
      <Empty />
      <LoginButton>Login</LoginButton>
      <SignupButton>Sign Up</SignupButton>
    </Container>
  )
}

export default Header;