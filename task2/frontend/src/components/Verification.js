import React from 'react';
import styled from 'styled-components/macro';
import { gql, useMutation } from '@apollo/client';

import tickImage from '../images/tick.svg';
import penImage from '../images/pen.svg';

const APPLY_EMAIL = gql`
  mutation Mutation($id: ID!, $email: String!, $code: String!) {
    mailApply(id: $id, email: $email, code: $code)
  }
`;

const Container = styled.div`
  width: 588px;
  height: 466px; 
  margin: 0 auto;
  text-align: center;
`;

const Tick = styled.img`
  width: 48px;
  height: 48px;
  margin: 0 auto;
`;

const Text1 = styled.div`
  font-style: normal;
  font-weight: 300;
  font-size: 14px;
  line-height: 20px;
  padding-top: 24px;
`;

const Email = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 24px;
  display: inline-block;
`;

const PenContainer = styled.div`
  display: inline-block;
  width: 24px;
  height: 24px;
  background: #E1E3E6;
  border-radius: 50%; 
  margin-left: 8px;
`;

const Pen = styled.img`
  width: 16px;
  height: 16px;
  margin-top: 2px;
`;

const TextContainer = styled.div`
  position: static;
  width: 588px;
  height: 266px;
  background: #FFFFFF;
  box-shadow: 0px 3px 7px rgba(1, 48, 36, 0.07);
  border-radius: 8px;
  margin-top: 24px;
`;

const TextInnerContainer = styled.div`
  padding: 40px 80px;
`;

const Text2 = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 28px;
`;

const Text3 = styled.div`
  font-weight: 300;
  font-size: 16px;
  line-height: 24px;
  padding: 24px 0;
`;

const Text4 = styled.div`
  font-weight: 300;
  font-size: 14px;
  line-height: 20px;
  padding: 24px 0;
`;

const GreenText = styled.span`
  color: #0A6629;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
`;

const NumbersContainer = styled.div`
  width: 216px;
  height: 62px;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
`;

const colors = {
  default: '#BEC1C4',
  valid: 'green',
  invalid: 'red'
}

const NumberContainer = styled.div`
  width: 48px;
  height: 62px;
  border-bottom: 2px solid ${({ state }) => colors[state]};
`;

const Number = styled.input.attrs(() => ({
  type: "text",
  size: "1",
  maxLength: "1"
}))`
  width: 16px;
  padding: 16px;
  margin: 0;
  border: 0;
  font-weight: 500;
  font-size: 24px;
  line-height: 28px;
`;

function Verification(props) {
  const { userState } = props;
  const [applyEmail] = useMutation(APPLY_EMAIL);
  const [numberState, setNumberState] = React.useState('default');
  const ref0 = React.useRef();
  const ref1 = React.useRef();
  const ref2 = React.useRef();
  const ref3 = React.useRef();

  const handleKeyUp = React.useCallback((e) => {
    const { name } = e.target;
    if (name === 'number0') {
      ref1.current.disabled = false;
      ref1.current.focus();
    }
    if (name === 'number1') {
      ref2.current.disabled = false;
      ref2.current.focus();
    }
    if (name === 'number2') {
      ref3.current.disabled = false;
      ref3.current.focus();
    }
    if (name === 'number3') {
      ref3.current.blur();
      const code = [ref0.current.value, ref1.current.value, ref2.current.value, ref3.current.value].join('');
      if (code.length === 4) {
        const variables = {
          id: userState.id,
          email: userState.email,
          code
        }
        applyEmail({ variables }).then(resp => {
          setNumberState(resp.data.mailApply ? 'valid' : 'invalid');
        });
      }
    }
  }, [applyEmail, userState]);

  React.useEffect(() => {
    ref0.current.focus();
  }, []);

  return (
    <Container>
      <Tick src={tickImage} />
      <br />
      <Text1>Verification email has been sent to</Text1>
      <br />
      <Email>{userState.email}</Email><PenContainer><Pen src={penImage} /></PenContainer>
      <TextContainer>
        <TextInnerContainer>
          <Text2>Verify your account</Text2>
          <Text3>Please, enter the code here or follow the link from the email. Link expires in <b>48 hours</b>. </Text3>
          <NumbersContainer>
            <NumberContainer state={numberState} ><Number ref={ref0} name="number0" onKeyUp={handleKeyUp} /></NumberContainer>
            <NumberContainer state={numberState} ><Number ref={ref1} name="number1" onKeyUp={handleKeyUp} disabled /></NumberContainer>
            <NumberContainer state={numberState} ><Number ref={ref2} name="number2" onKeyUp={handleKeyUp} disabled /></NumberContainer>
            <NumberContainer state={numberState} ><Number ref={ref3} name="number3" onKeyUp={handleKeyUp} disabled /></NumberContainer>
          </NumbersContainer>
        </TextInnerContainer>
      </TextContainer>
      <Text4>Haven’t got an email? Check spam folder or <GreenText>Resend</GreenText></Text4>
    </Container>
  )
}

export default Verification;