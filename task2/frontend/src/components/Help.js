import styled from 'styled-components/macro';
import chatImage from '../images/chat.svg';
import emailImage from '../images/email.svg';
import questionImage from '../images/question.svg';

const Container = styled.div`
  width: 636px;
  height: 220px;
  margin: 0 auto 80px;
  text-align: center;
`;

const Title = styled.span`
  font-weight: 600;
  font-size: 20px;
  line-height: 24px;
  margin: 0 auto;
`;

const CardsContainer = styled.div`
  display: flex;
  justify-content: space-around;
  margin: 24px 0;
`;

const Card = styled.div`
  background: #FFFFFF;
  box-shadow: 0px 3px 7px rgba(1, 48, 36, 0.07);
  border-radius: 8px;
  width: 180px;
  height: 108px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  cursor: pointer;
`;

const CardText = styled.span`
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  padding-bottom: 5px;
`;

const Image = styled.img`
  width: 40px;
  height: 40px;
  margin: 0 auto;
  padding-top: 10px;
`;

const BottomText = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 24px;
  color: #87898C;
`;

function Help() {
  return (
    <Container>
      <Title>Need help?</Title>
      <CardsContainer>
        <Card>
          <Image src={chatImage} />
          <CardText>Chat</CardText>
        </Card>
        <Card>
          <Image src={emailImage} />
          <CardText>Email Us</CardText>
        </Card>
        <Card>
          <Image src={questionImage} />
          <CardText>Ask a question</CardText>
        </Card>
      </CardsContainer>
      <BottomText>Call Us: 1 855 932 4048</BottomText>
      <br />
      <BottomText>Monday – Friday, 8:00 AM to 5:00 PM EST </BottomText>
    </Container>
  )
}

export default Help;