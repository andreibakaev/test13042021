import React from 'react';
import styled from 'styled-components/macro';

import { gql, useMutation } from '@apollo/client';

import closeImage from '../images/close.svg';
import Mask from './Mask';

const CHANGE_EMAIL = gql`
  mutation Mutation($id: ID, $email: String!) {
    mailChange(id: $id, email: $email) {
      id
      email
    }
  }
`;

const Container = styled.div`
  background: #FFFFFF;
  box-shadow: 0px 16px 32px rgba(0, 0, 0, 0.08), 0px 8px 24px rgba(0, 0, 0, 0.06), 0px 4px 12px rgba(0, 0, 0, 0.04), 0px 4px 4px rgba(0, 0, 0, 0.02);
  border-radius: 8px;
  width: 520px;
  height: 364px;
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`;

const InnerContainer = styled.div`
  padding: 16px;
`;

const CloseButtonContainer = styled.div`
  height: 24px;
  text-align: right;
`;

const Close = styled.img`
  width: 24px;
  height: 24px;
  cursor: pointer;
`;

const FormContainer = styled.div`
  width: 360px;
  padding: 40px 0px;
  margin: 0 auto;
`;

const Text1 = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 28px;
  text-align: center;
`;

const Text2 = styled.div`
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 20px;
  padding: 16px 0;
`;

const InputContainer = styled.fieldset`
  font-weight: normal;
  font-size: 11px;
  line-height: 16px;
  border: 1.5px solid #BEC1C4;
  border-radius: 8px;
  height: 56px;
`;

const EmailInput = styled.input`
  margin: 0;
  padding: 0;
  border: 0;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 20px;
  height: 100%;
  width: 100%; 
`;

const SaveButton = styled.button`
  width: 100%;
  background: #0A6629;
  border-radius: 8px;
  margin-top: 16px;
  padding: 16px;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
  color: #fff;
  cursor: pointer;
  &[disabled] {
    background: #ccc;
    cursor: not-allowed;
  }
`;

function EmailDialog(props) {
  const { onChangeEmail, userState } = props;
  const [email, setEmail] = React.useState();
  const [changeEmail] = useMutation(CHANGE_EMAIL);

  const handleSaveClick = React.useCallback(() => {
    const variables = { email };
    if (userState && userState.id) {
      variables.id = userState.id;
    }
    changeEmail({ variables }).then(resp => {
      onChangeEmail(resp.data.mailChange);
    });
  }, [changeEmail, userState, onChangeEmail, email]);

  return (
    <>
      <Mask />
      <Container>
        <InnerContainer>
          <CloseButtonContainer><Close src={closeImage} /></CloseButtonContainer>
          <FormContainer>
            <Text1>
              Change Email
          </Text1>
            <Text2>Enter new e-mail </Text2>
            <InputContainer>
              <legend>New e-mail</legend>
              <EmailInput onChange={(e) => setEmail(e.target.value)} value={email} />
            </InputContainer>
            <SaveButton onClick={handleSaveClick} disabled={!email?.includes('@')} type="button">Save</SaveButton>
          </FormContainer>
        </InnerContainer>
      </Container>
    </>
  )
}

export default EmailDialog;
