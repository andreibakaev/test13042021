import styled from 'styled-components/macro';

import tickImage from '../images/tick.svg';
import Mask from './Mask';

const Container = styled.div`
  background: #FFFFFF;
  box-shadow: 0px 16px 32px rgba(0, 0, 0, 0.08), 0px 8px 24px rgba(0, 0, 0, 0.06), 0px 4px 12px rgba(0, 0, 0, 0.04), 0px 4px 4px rgba(0, 0, 0, 0.02);
  border-radius: 8px;
  width: 520px;
  height: 316px;
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`;

const InnerContainer = styled.div`
  padding: 16px;
`;

const CloseButtonContainer = styled.div`
  height: 24px;
  text-align: right;
`;

const FormContainer = styled.div`
  width: 360px;
  padding: 40px 0px;
  margin: 0 auto;
  text-align: center;
`;

const Tick = styled.img`
  width: 48px;
  height: 48px;
  margin: 0 auto;
`;

const Text1 = styled.div`
  font-weight: 500;
  font-size: 24px;
  line-height: 28px;
  white-space: nowrap;
  margin: 16px 0;
`;

const Text2 = styled.div`
  font-weight: 300;
  font-size: 16px;
  line-height: 24px;
`;

function SuccessDialog() {
  return (
    <>
      <Mask />
      <Container>
        <InnerContainer>
          <CloseButtonContainer />
          <FormContainer>
            <Tick src={tickImage} />
            <Text1>Your email has been changed!</Text1>
            <Text2>Please, check you inbox and follow the link in order to verify your email.</Text2>
          </FormContainer>
        </InnerContainer>
      </Container>
    </>
  )
}

export default SuccessDialog;