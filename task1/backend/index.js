const { v4: uuidv4 } = require('uuid');
const express = require('express');
const cors = require('cors');
const app = express();
const port = 4000;

app.use(cors({ origin: true }));
app.use(express.json());

reviewRoutes(app);

app.listen(port, () => {
  console.log(`---express--- http://localhost:${port}`);
});

const reviews = [
  {
    id: uuidv4(),
    reviewText: 'test reviewText',
    reviewText1: 'test reviewText 1',
    reviewText2: 'test reviewText 2',
    reviewText3: 'test reviewText 3',
  }
];

function all(req, res) {
  return res.status(201).send(reviews);
}

function create(req, res) {
  const { body } = req;
  if (body) {
    const newReview = {
      ...body,
      id: uuidv4()
    };
    reviews.push(newReview);
    return res.status(201).send(newReview);
  }
  return res.status(405).send({ status: 'Not Allowed' });
}

function reviewRoutes(_app) {
  _app.get('/review/query', all);
  _app.post('/review/create', create);
}
