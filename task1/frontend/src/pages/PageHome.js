import styled from 'styled-components/macro';
import { Link } from 'react-router-dom';

const Button = styled(Link)`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  background: #0A6629;
  border-radius: 8px;
  padding: 16px;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
  color: #fff;
  cursor: pointer;
  text-decoration: none;
`;

function PageHome() {
  return (
    <Button to="/review">Make Review</Button>
  )
}

export default PageHome;