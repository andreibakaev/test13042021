import styled from 'styled-components/macro';
import mealImage from '../images/mealbig.svg';
import startsImage from '../images/stars.svg'

const Container = styled.div`
  display: flex;
  height: 280px;
`;

const Container2 = styled.div`
  width: 234px;
  height: 100%;
  border: 1px solid #E1E3E6;
  border-radius: 8px;
`;

const InnerContainer = styled.div`
  padding: 16px;
`;

const Text = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
  margin: 16px 0;
`;

const Image = styled.img`
  height: 151px;
`;

const Stars = styled.img`
  height: 24px;
`;

const TextRate = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 16px;
  color: #744500;
  position: relative;
  top: -10px;
  left: 5px;
`;

function MealReviewRow(props) {
  const { children } = props;
  return (
    <Container>
      <Container2>
        <InnerContainer>
          <Image src={mealImage} />
          <Text>Morroccan Chicken With Couscous</Text>
          <Stars src={startsImage} />
          <TextRate>3/5</TextRate>
        </InnerContainer>
      </Container2>
      {children}
    </Container>
  )
}

export default MealReviewRow;