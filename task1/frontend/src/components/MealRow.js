import styled from 'styled-components/macro';
import starsImage from '../images/stars.svg';
const Container = styled.div`
  height: 72px;
  display: flex;
`;

const Image = styled.img`
  filter: drop-shadow(0px 16px 32px rgba(0, 0, 0, 0.08)), drop-shadow(0px 8px 24px rgba(0, 0, 0, 0.06)), drop-shadow(0px 4px 12px rgba(0, 0, 0, 0.04)), drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.02));
  border-radius: 8px;
  width: 72px;
`;

const Stars = styled.img`
  height: 24px;
`;

const Container2 = styled.div`
  width: 136px;

`;

const Text = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
`;

function MealRow(props) {
  const { text, image, children } = props;
  return (
    <Container>
      <Image src={image} />
      <Container2>
        <Text>{text}</Text>
        <Stars src={starsImage} />
      </Container2>
      {children}
    </Container>
  )
};

export default MealRow;