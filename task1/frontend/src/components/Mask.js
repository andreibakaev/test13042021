import styled from 'styled-components/macro';

const Mask = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  opacity: 0.8;
  background: #fff;
`;

export default Mask;