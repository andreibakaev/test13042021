import styled from 'styled-components/macro';

import { createEffect, createStore, createEvent, sample } from 'effector'
import { useStoreMap } from 'effector-react'

import Mask from '../components/Mask';
import closeImage from '../images/close.svg';
import fileImage from '../images/file.svg';
import penImage from '../images/greenpen.svg';

import MealRow from './MealRow';
import MealReviewRow from './MealReviewRow';

import mealImage0 from '../images/meal0.svg';
import mealImage1 from '../images/meal1.svg';
import mealImage2 from '../images/meal2.svg';

const Container = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 776px;
  height: 1177.5px;
  max-height: 95vh;
  background: #FFFFFF;
  box-shadow: 0px 16px 32px rgba(0, 0, 0, 0.08), 0px 8px 24px rgba(0, 0, 0, 0.06), 0px 4px 12px rgba(0, 0, 0, 0.04), 0px 4px 4px rgba(0, 0, 0, 0.02);
  border-radius: 8px;
  overflow-y: auto;
`;

const InnerContainer = styled.div`
  padding: 16px;
`;

const CloseButtonContainer = styled.div`
  height: 24px;
  text-align: right;
`;

const Close = styled.img`
  width: 24px;
  height: 24px;
  cursor: pointer;
`;

const FormContainer = styled.div`
  width: 640px;
  padding: 16px 0px;
  margin: 0 auto;
`;

const Text1 = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 28px;
  text-align: center;
`;

const Text2 = styled.div`
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 20px;
  padding: 16px 0;
`;

const Input = styled.input.attrs(() => ({
  placeholder: 'Your thoughts about the component'
}))`
  margin: 0;
  padding: 0;
  background: #FFFFFF;
  border: 1.5px solid #BEC1C4;
  box-sizing: border-box;
  border-radius: 8px;
  height: 56px;
  width: 390px;
  margin-left: 40px;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 20px;
  color: #87898C;
  padding: 18px;
`;

const Hr = styled.hr`
  margin: 32px 0px;
`;

const TextArea = styled.textarea.attrs(() => ({
  placeholder: 'Meal Summary Review'
}))`
  width: 390px;
  height: 282px;
  padding: 18px;
  margin-left: 16px;
  border: 1.5px solid #BEC1C4;
  box-sizing: border-box;
  border-radius: 8px;
  font-family: Sora;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 20px;
  color: #87898C;
`;

const Text3 = styled.div`
  font-style: normal;
  font-weight: 300;
  font-size: 14px;
  line-height: 20px;
`;

const File = styled.div`
  height: 57px;
  border-radius: 8px;
  border: 2px dotted #ccc;
  margin-top: 16px;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
  color: #87898C;
  display: flex;
  align-items: center;
  justify-content: space-around;
`;

const InputContainer = styled.fieldset`
  font-weight: normal;
  font-size: 11px;
  line-height: 16px;
  border: 1.5px solid #BEC1C4;
  border-radius: 8px;
  height: 56px;
  margin: 32px 0;
  position: relative;
`;

const NicknameInput = styled.input`
  margin: 0;
  padding: 0;
  border: 0;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 20px;
  height: 100%;
  width: 100%; 
`;

const Pen = styled.img`
  position: absolute;
  top: 10px;
  right: 16px;
  width: 24px;
  height: 24px;
`;

const Checkbox = styled.input`

`;

const Text4 = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 16px;
  color: #87898C;
  margin-left: 12px;
`;

const GreenLink = styled.a`
  color: #34A34F;
  text-decoration: underline;
`;

const Button = styled.button`
  background: #0A6629;
  border-radius: 8px;
  padding: 16px;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
  color: #fff;
  cursor: pointer;
  width: 100%;
  border: 0;
  margin-top: 40px;
  cursor: pointer;
`;

const postData = async values => {
  const response = await fetch('http://localhost:4000/review/create', {
    method: 'POST',
    body: JSON.stringify(values),
    headers: { 'Content-Type': 'application/json' }
  });
  const json = await response.json();
  console.log('---json response---', json);
}

const fxReviewCreate = createEffect(values => {
  const { nickname, ...other } = values;
  postData(other);
})

const submitted = createEvent();
const setField = createEvent();
const $form = createStore({}).on(setField, (s, { key, value }) => ({
  ...s,
  [key]: value,
}));

sample({
  clock: submitted,
  source: $form,
  target: fxReviewCreate,
});

const handleChange = setField.prepend(e => ({
  key: e.target.name,
  value: e.target.value,
}));

submitted.watch(e => {
  e.preventDefault()
});

const Field = ({ name, component: InputComponent }) => {
  const value = useStoreMap({
    store: $form,
    keys: [name],
    fn: values => values[name] || '',
  })
  return <InputComponent name={name} value={value} onChange={handleChange} />
}

function ReviewDialog() {
  return (
    <>
      <Mask />
      <Container>
        <InnerContainer>
          <CloseButtonContainer><Close src={closeImage} /></CloseButtonContainer>
          <form onSubmit={submitted}>
            <FormContainer>
              <Text1>Meal Review</Text1>
              <Text2>Meal Components</Text2>
              <MealRow text="Morrocan Chicken" image={mealImage0}><Field name="reviewText" component={Input} /></MealRow>
              <MealRow text="Couscous Pilaf" image={mealImage1}><Field name="reviewText1" component={Input} /></MealRow>
              <MealRow text="Turkey Meatloaf" image={mealImage2}><Field name="reviewText2" component={Input} /></MealRow>
              <Hr />
              <Text2>Meal Review</Text2>
              <MealReviewRow><Field name="reviewText3" component={TextArea} /></MealReviewRow>
              <br />
              <br />
              <Text3>Add the meal photos</Text3>
              <File><img src={fileImage} alt="file" /><span>Upload photos or drop them here</span></File>
              <InputContainer>
                <legend>Your Nickname (other users will see this)</legend>
                <Field name="nickname" component={NicknameInput} />
                <Pen src={penImage} />
              </InputContainer>
              <div>
                <Checkbox type="checkbox" />
                <Text4>I confirm that I have read and accepted <GreenLink>Terms and Conditions</GreenLink> and <GreenLink>Privacy Policy</GreenLink></Text4>
              </div>
              <Button type="submit">Submit Review</Button>
            </FormContainer>
          </form>
        </InnerContainer>
      </Container>
    </>
  )
}

export default ReviewDialog;

