import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { ThemeProvider } from 'styled-components/macro';
import { theme, GlobalStyle } from './theme';

import PageHome from './pages/PageHome';
import PageReview from './pages/PageReview';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Switch>
          <Route path="/review">
            <PageReview />
          </Route>
          <Route path="/">
            <PageHome />
          </Route>
        </Switch>
      </Router>
      <GlobalStyle />
    </ThemeProvider>
  )
}

export default App;

