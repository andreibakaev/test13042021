import { createGlobalStyle } from 'styled-components/macro';

export const theme = {


};

export const GlobalStyle = createGlobalStyle`
  html, body {
    width: 100%;
    height: 100vh;
    height: calc(var(--vh, 1vh) * 100);
    margin: 0;
    overflow: hidden;
    background-color: #E5E5E5;
    position: fixed;
    color: #000;
    font-size: 14px;
    font-family: Sora;
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 20px;
    color: #252728;
  }

  #root {
    height: 100%;
    width: 100%;
  }
`;